#include <Arduino.h>

// Define D5 as Input Pin
const int INPUT_PIN = D5;
int value = 0;

// Setup
void setup()
{
  Serial.begin(115200);
  pinMode(INPUT_PIN, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

// Loop
void loop()
{
  // Read
  value = digitalRead(INPUT_PIN);
  // Log
  Serial.print("\t Digital Reading=");
  Serial.println(value);
  // LED
  if (value == LOW)
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
  }
  // Wait
  delay(500);
}