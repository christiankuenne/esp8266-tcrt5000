# ESP8266 with TCRT5000 IR Sensor

Hello world example for ESP8266 with TCRT5000 IR sensor.

## Wiring

(ESP to TCRT5000)

- Ground: GND to G
- Power: 3V3 to V+
- Data: D5 to S
